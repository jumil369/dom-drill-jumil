let minTens = 0
let hourOnes = 0
let secondTens = 0
let secondOnes = 0
let msHundreds = 0
let msTens = 0
var timer = null

window.onload = function () {
  // for  controller the timer
  let controlDiv = document.createElement('DIV')
  controlDiv.style.padding = '2rem'
  controlDiv.style.margin = '2rem'
  controlDiv.style.backgroundColor = 'yellow'
  document.body.appendChild(controlDiv)

  let start = document.createElement('BUTTON')
  start.innerHTML = 'Start'
  start.id = 'start'

  controlDiv.appendChild(start)

  let stop = document.createElement('BUTTON')
  stop.innerHTML = 'Stop'
  stop.style.margin = '2rem'
  stop.id = 'stop'

  controlDiv.appendChild(stop)

  let reset = document.createElement('BUTTON')
  reset.innerHTML = 'Reset'
  reset.id = 'reset'
  controlDiv.appendChild(reset)

  document.getElementById('start').disabled = false
  document.getElementById('stop').disabled = true
  document.getElementById('reset').disabled = true

  // Event Listener START, STOP AND RESET
  start.addEventListener('click', () => {
    timerStart()
    document.getElementById('start').disabled = true
    document.getElementById('stop').disabled = false
    document.getElementById('reset').disabled = false
  })

  stop.addEventListener('click', () => {
    clearInterval(timer)
    timer = null
    document.getElementById('start').disabled = false
    document.getElementById('stop').disabled = true
    document.getElementById('reset').disabled = false
  })

  reset.addEventListener('click', () => {
    clearInterval(timer)
    timer = null
    minTens = 0
    hourOnes = 0
    secondTens = 0
    secondOnes = 0
    msHundreds = 0
    msTens = 0
    document.getElementById('minTens').innerHTML = 0
    document.getElementById('hourOnes').innerHTML = 0
    document.getElementById('secondTens').innerHTML = 0
    document.getElementById('secondOnes').innerHTML = 0
    document.getElementById('msHundreds').innerHTML = 0
    document.getElementById('msTens').innerHTML = 0
    document.getElementById('secondTens').style.color = 'BLACK'
    document.getElementById('secondOnes').style.color = 'BLACK'

    document.getElementById('start').disabled = false
    document.getElementById('stop').disabled = true
    document.getElementById('reset').disabled = true
  })
}

// Timer part
function timerStart () {
  timer = setInterval(function () {
    msTens++
    if (msTens > 9) {
      msTens = 0
      msHundreds++
    }
    if (msHundreds > 5 && msTens == 0) {
      msHundreds = 0
      secondOnes++
      document.getElementById('secondTens').style.color = 'RED'
      document.getElementById('secondOnes').style.color = 'RED'
    }
    if (secondOnes > 9 && msHundreds == 0 && msTens == 0) {
      secondOnes = 0
      secondTens++
    }
    if (secondTens > 5 && secondOnes == 0 && msHundreds == 0 && msTens == 0) {
      secondTens = 0
      hourOnes++
    }
    if (hourOnes > 9 && secondTens == 0 && secondOnes == 0 && msHundreds == 0 && msTens == 0) {
      hourOnes = 0
      minTens++
    }
    if (minTens > 5 && hourOnes == 0 && secondTens == 0 && secondOnes == 0 && msHundreds == 0 && msTens == 0) {
      minTens = 0
    }
    document.getElementById('minTens').innerHTML = minTens
    document.getElementById('hourOnes').innerHTML = hourOnes
    document.getElementById('secondTens').innerHTML = secondTens
    document.getElementById('secondOnes').innerHTML = secondOnes
    document.getElementById('msHundreds').innerHTML = msHundreds
    document.getElementById('msTens').innerHTML = msTens
  }, 10)
}
