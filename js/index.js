const siteContent = {
    "nav": {
        "nav-item-1": "Services",
        "nav-item-2": "Product",
        "nav-item-3": "Vision",
        "nav-item-4": "Features",
        "nav-item-5": "About",
        "nav-item-6": "Contact",
        "img-src": "img/logo.png"
    },
    "cta": {
        "h1": "DOM Is Awesome",
        "button": "Get Started",
        "img-src": "img/header-img.png"
    },
    "main-content": {
        "features-h4": "Features",
        "features-content": "Features content elementum magna eros, ac posuere elvit tempus et. Suspendisse vel tempus odio, in interdutm nisi. Suspendisse eu ornare nisl. Nullam convallis augue justo, at imperdiet metus scelerisque quis.",
        "about-h4": "About",
        "about-content": "About content elementum magna eros, ac posuere elvit tempus et. Suspendisse vel tempus odio, in interdutm nisi. Suspendisse eu ornare nisl. Nullam convallis augue justo, at imperdiet metus scelerisque quis.",
        "middle-img-src": "img/mid-page-accent.jpg",
        "services-h4": "Services",
        "services-content": "Services content elementum magna eros, ac posuere elvit tempus et. Suspendisse vel tempus odio, in interdutm nisi. Suspendisse eu ornare nisl. Nullam convallis augue justo, at imperdiet metus scelerisque quis.",
        "product-h4": "Product",
        "product-content": "Product content elementum magna eros, ac posuere elvit tempus et. Suspendisse vel tempus odio, in interdutm nisi. Suspendisse eu ornare nisl. Nullam convallis augue justo, at imperdiet metus scelerisque quis.",
        "vision-h4": "Vision",
        "vision-content": "Vision content elementum magna eros, ac posuere elvit tempus et. Suspendisse vel tempus odio, in interdutm nisi. Suspendisse eu ornare nisl. Nullam convallis augue justo, at imperdiet metus scelerisque quis.",
    },
    "contact": {
        "contact-h4": "Contact",
        "address": "123 Way 456 Street Somewhere, USA",
        "phone": "1 (888) 888-8888",
        "email": "sales@greatidea.io",
    },
    "footer": {
        "copyright": "Copyright Great Idea! 2018"
    },
};

// nav
document.getElementById("logo-img").setAttribute('src', siteContent["nav"]["img-src"]);

let nav = document.querySelectorAll('nav > a');
for (let i = 0; i < nav.length; i++) {
    nav[i].innerHTML = siteContent["nav"]["nav-item-" + (i + 1)]
    nav[i].style.color = "green";
}

//nav -> using .prepend
let navLinkFirst = document.createElement("a");
navLinkFirst.innerHTML = "Home";
navLinkFirst.style.color = "green";
document.querySelector('nav').prepend(navLinkFirst);

//nav -> using .prepend
let navLinkSecond = document.createElement("a");
navLinkSecond.innerHTML = "Logout";
navLinkSecond.style.color = "green";
document.querySelector('nav').appendChild(navLinkSecond);



// document.querySelector('nav').prepend('<a href="#">Logout</a>');


//cta
let headText = document.querySelector('.cta-text > h1');
let textArray = siteContent["cta"]["h1"].split(" ");
headText.innerHTML = textArray[0] + "<br> " + textArray[1] + "<br>" + textArray[2];
document.querySelector('.cta-text > button').innerHTML = siteContent["cta"]["button"];
document.getElementById("cta-img").setAttribute('src', siteContent["cta"]["img-src"]);

//main-content 
document.getElementById("middle-img").setAttribute('src', siteContent["main-content"]["middle-img-src"]);

//main-content -> top-content
let topContent = document.querySelectorAll('.top-content > .text-content');
topContent[0].querySelector("h4").innerHTML = siteContent["main-content"]["features-h4"];
topContent[0].querySelector("p").innerHTML = siteContent["main-content"]["features-content"];
topContent[1].querySelector("h4").innerHTML = siteContent["main-content"]["about-h4"];
topContent[1].querySelector("p").innerHTML = siteContent["main-content"]["about-content"];

//main-content -> bottom-content
let bottomContent = document.querySelectorAll('.bottom-content > .text-content');
bottomContent[0].querySelector("h4").innerHTML = siteContent["main-content"]["services-h4"];
bottomContent[0].querySelector("p").innerHTML = siteContent["main-content"]["services-content"];
bottomContent[1].querySelector("h4").innerHTML = siteContent["main-content"]["product-h4"];
bottomContent[1].querySelector("p").innerHTML = siteContent["main-content"]["product-content"];
bottomContent[2].querySelector("h4").innerHTML = siteContent["main-content"]["vision-h4"];
bottomContent[2].querySelector("p").innerHTML = siteContent["main-content"]["vision-content"];

//contact
document.querySelector('.contact > h4').innerHTML = siteContent["contact"]["contact-h4"];
let contact = document.querySelectorAll('.contact > p');
let address = siteContent["contact"]["address"].split(" ");
contact[0].innerHTML = address[0] + " " + address[1] + " " + address[2] + " " + address[3] + "<br>" + address[4] + " " + address[5];
contact[1].innerHTML = siteContent["contact"]["phone"];
contact[2].innerHTML = siteContent["contact"]["email"];

//footer
document.querySelector('footer > p').innerHTML = siteContent["footer"]["copyright"];